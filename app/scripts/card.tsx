import React, { useState } from 'react';
import "../styles/card.scss";

export default function RepoCard(props) {
  const [repo,setRepo]=useState(props.repo);
  // console.log(repo);
  if(repo.description)
    return (
      <div className="card-wrapper--outer">
        <div className="card-wrapper has-overlay">
          <div className="card">
            <p>{repo.owner.login}</p>
            <p>{repo.name}</p>
          </div>
          <div className="card-overlay-wrapper">
            <div className="card-overlay">
              <p>{repo.name}</p>
              <p>{repo.description}</p>
            </div>
          </div>
        </div>
      </div>
    );
  else
    return (
      <div className="card-wrapper--outer">
        <div className="card-wrapper">
          <div className="card">
            <p>{repo.owner.login}</p>
            <p>{repo.name}</p>
          </div>
        </div>
      </div>
    );
}