const gulp = require("gulp"),
  bs = require("browser-sync").create(),
  express = require('express');
const path = require("path");
class Server {
  constructor() {}
  start(cb) {
    bs.init({
      server: {
        baseDir: "./public",
      },
    });
    cb();
  }
  reload(cb) {
    bs.reload();
    cb();
  }
  prodStart(){
    const app=express();
    console.log(path.join(__dirname, 'dist'));
    app.use('/github-api', express.static(path.join(__dirname, '../dist')));
    app.listen(3000, () => {
      console.log(`Server listening on port 3000`)
    })
  }
}

module.exports = Server;
