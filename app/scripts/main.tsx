import App from "./app";
import React from 'react';
import ReactDOM from 'react-dom';
import Network from "./network";

class GithubList {
  network;
  data;
  constructor() {
    this.network=new Network();
    this.fetchData();
  }
  async fetchData(){
    let date=new Date();
    date.setDate(date.getDate()-7);
    this.data= await this.network.get("https://api.github.com/search/repositories?q=created:>"+date.toISOString().substring(0,date.toISOString().indexOf("T"))+"&per_page=48&sort=updated&order=desc");
    ReactDOM.render(
      <App data={this.data} />,
      document.getElementById('component'),
    );
  }
}

new GithubList();