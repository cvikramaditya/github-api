import React from 'react';
import renderer from 'react-test-renderer';
import App from '../../scripts/app';
import dataSample from "../../../mock-data.json";

test('Add App Component', () => {
  const component = renderer.create(
    <App data={dataSample}/>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});