import axios from 'axios';

export default class Network {
  constructor() {}
  async get(url){
    let data=await axios.get(url);
    return data.data;
  }
}