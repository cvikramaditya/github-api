import 'regenerator-runtime/runtime';
import Network from '../../scripts/network';
import axios from 'axios';
const mockedAxios = axios as jest.Mocked<typeof axios>;

mockedAxios.get.mockRejectedValue('Network error: Something went wrong');
mockedAxios.get.mockResolvedValue({ data: {} });

it('make api call to get data', () => {
  let net=new Network();
   net.get("https://api.github.com/search/repositories?q=created:>2021-05-01&per_page=10&sort=help-wanted-issues&order=desc");
   expect(mockedAxios.get).toHaveBeenCalledWith("https://api.github.com/search/repositories?q=created:>2021-05-01&per_page=10&sort=help-wanted-issues&order=desc")
})