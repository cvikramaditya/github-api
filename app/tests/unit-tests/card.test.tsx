import React from 'react';
import renderer from 'react-test-renderer';
import RepoCard from '../../scripts/card';
import dataSample from "../../../mock-data.json";

test('Add App Component', () => {
  const component = renderer.create(
    <RepoCard  repo={dataSample.items[0]} key={0} spacing={3}/>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});