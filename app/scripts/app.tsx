import React, { useState } from 'react';
import RepoCard from "./card";
import "../styles/app.scss";

export default function App(props) {
  const [data,setData]=useState(props.data);
  return (
    <div className="github-repo-list">
        {data.items.map((x, index) => <RepoCard repo={x} key={index} />)}
    </div>
  );
}